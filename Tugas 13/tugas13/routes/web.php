<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\castController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/table',function(){
    return view('page.table');
});

Route::get('/data-table', function(){
    return view('page.data-table');
});

Route::get('/master', function(){
    return view('layouts.master');
});




Route::get('/cast',[castController::class, 'index']);
Route::get('/cast/create',[castController::class, 'create']);
Route::post('/cast',[castController::class, 'store']);
Route::get('/cast/{cast_id}',[castController::class, 'show']);
Route::get('/cast/{cast_id}/edit',[castController::class, 'edit']);
Route::put('/cast/{cast_id}',[castController::class, 'update']);
Route::delete('/cast/{cast_id}',[castController::class, 'destroy']);

