<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class castController extends Controller
{
    public function create(){
        return view('castsView.tambah');
    }

    public function store(Request $request){
        $request->validate([
            'nama'=>'required|max:255',
            'umur'=>'required|integer',
            'bio'=>'required'
        ]);
        DB::table('castable')->insert([
            'nama' =>$request->input('nama'),
            'umur' =>$request->input('umur'),
            'bio'=>$request->input('bio')
        ]);

        return redirect('/cast');
    }

    public function index(){

        $data = DB::table('castable')->get();

        return view('castsView.lihat',['data'=>$data]);
    }

    public function show($id){
        $data = DB::table('castable')->find($id);

        return view('castsView.detail',['data'=>$data]);
    }

    public function edit($id){
        $data = DB::table('castable')->find($id);

        return view('castsView.ubah',['data'=>$data]);
    }

    public function update($id, Request $request){
        $request->validate([
            'nama'=>'required|max:255',
            'umur'=>'required|integer',
            'bio'=>'required'
        ]);
        DB::table('castable')->where('id', $id)
            ->update([
                'nama' =>$request->input('nama'),
                'umur' =>$request->input('umur'),
                'bio'=>$request->input('bio')
            ]);

        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('castable')->where('id','=',$id)->delete();
        return redirect('/cast');
    }
}
