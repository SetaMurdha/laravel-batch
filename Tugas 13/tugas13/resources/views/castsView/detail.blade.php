@extends('layouts.master')

@section('title')
Bio Data
@endsection

@section('content')
<h1>{{$data->nama}}</h1>
<p>{{$data->bio}}</p>
@endsection