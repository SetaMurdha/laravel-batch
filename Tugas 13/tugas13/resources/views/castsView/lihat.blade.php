@extends('layouts.master')

@section('title')
Lihat Data
@endsection

@section('content')
    <table class="table">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Update</th>
        <th scope="col">Hapus</th>
        </tr>
    </thead>
    <tbody>
        @forelse($data as $key=> $value)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td><a class="btn btn-primary" href="/cast/{{$value->id}}">Bio</a></td>
            <td><a class="btn btn-primary" href="/cast/{{$value->id}}/edit">Update</a></td>
            <td>
            <form action="/cast/{{$value->id}}" method="post">
                @csrf
                @method('delete')
                <input type="submit" value="Delete" class="btn btn-primary" >
            </form>
            </td>


            
        </tr>
        @empty
            <tr>
                <td>Data kosong</td>
            </tr>
        @endforelse
        
    </tbody>
    </table>
@endsection