@extends('layouts.master')

@section('title')
Tambah Data
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
    <div class="mb-3">
    <label for="exampleFormControlInput1" class="form-label">Nama</label>
    <input type="text" class="form-control @error('nama') is_invalid @enderror" name="nama" >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    
    <div class="mb-3">
    <label for="exampleFormControlInput1" class="form-label">Umur</label>
    <input type="text" class="form-control @error('umur') is_invalid @enderror" name="umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="mb-3">
    <label for="exampleFormControlTextarea1" class="form-label">Bio</label>
    <textarea class="form-control @error('bio') is_invalid @enderror" rows="3" name="bio"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
