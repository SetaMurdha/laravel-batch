<?php

require ('animal.php');
require ('ape.php');
require ('frog.php');

// Release 0

$sheep = new Animal("shaun");

echo "Nama : ".$sheep->name. "<br>"; // "shaun"
echo "Legs : ".$sheep->legs. "<br>"; // 4
echo "Cold Blooded : ".$sheep->cold_blooded. "<br><br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

// Release 1

// index.php
$sungokong = new Ape("kera sakti");
echo "Nama : ".$sungokong->name. "<br>"; // "shaun"
echo "Legs : ".$sungokong->legs. "<br>"; // 4
echo "Cold Blooded : ".$sungokong->cold_blooded. "<br>"; // "no"
echo "Yell : ";
$sungokong->yell(); // "Auooo"
echo "<br>";

$kodok = new Frog("buduk");
$kodok->name;
echo "Nama : ".$kodok->name. "<br>"; // "shaun"
echo "Legs : ".$kodok->legs. "<br>"; // 4
echo "Cold Blooded : ".$kodok->cold_blooded. "<br>"; // "no"
echo "Jump : ";
$kodok->jump() ; // "hop hop"

