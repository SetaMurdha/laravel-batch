<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view('regis');
    }

    public function kirim(Request $request){
        $namaDepan = $request->input('first-nama');
        $namaBelakang = $request->input('last-nama');
        // $gender = $request->input('gender');
        // $nationality = $request->input('nationality');
        // $languageSpoken = $request->input('language-spoken');
        // $biography = $request->input('biography');

        return view('selamat_datang', ['namaDepan'=>$namaDepan, 'namaBelakang'=>$namaBelakang]);
    }
}
