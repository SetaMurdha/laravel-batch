<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Akun Baru!</h1>
    <h3>Sign up form</h3>
    <form action="/daftar" method="POST">
        @csrf
        <label>Nama Depan</label><br>
        <input type="text" name="first-nama"><br><br>
        <label>Nama Belakang</label><br>
        <input type="text" name="last-nama"><br><br>

        <label>Gender</label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>

        <label>Nationality</label><br>
        <select name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="USA">USA</option>
            <option value="Malaysia">Malaysia</option>
        </select><br><br>

        <label>Language Spoken:</label><br>
        <input type="checkbox" name="language-spoken">Bahasa Indonesia<br>
        <input type="checkbox" name="language-spoken">English<br>
        <input type="checkbox" name="language-spoken">Others<br><br>

        <label>Bio:</label><br>
        <textarea name="biography"cols="30" rows="10"></textarea><br><br>

        <input type="submit">
    </form>
</body>
</html>